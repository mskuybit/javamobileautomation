package sample;

public class TestClass {
    public static void main(String[] args) {
        Parent parent = new Parent();

        //parent.sum();

        Child child = new Child();

        child.sum();
    }
}

/*
* private - access only inside class
* package - private(default) - access only inside package
* protected - access only inside package + descendants
* public - access from anywhere
* */
